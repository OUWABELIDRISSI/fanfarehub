from django.apps import AppConfig


class BaseConfig(AppConfig):
    name = 'fanfarehub.base'
    verbose_name = "Base"

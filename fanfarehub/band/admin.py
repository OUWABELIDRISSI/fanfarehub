from django.contrib import admin

from .models import Instrument, Stand


@admin.register(Instrument)
class InstrumentAdmin(admin.ModelAdmin):
    pass


@admin.register(Stand)
class StandAdmin(admin.ModelAdmin):
    pass
from django.db import models 
from django.utils.translation import gettext
from django.utils.translation import gettext_lazy as _
from ..accounts.models import User 

class Note(models.TextChoices):
    """Names of the 12 notes of a chromatic scale."""

    C = 'C', _("C")
    Cd = 'Cd', _("C♯")
    Db = 'Db', _("D♭")
    D = 'D', _("D")
    Dd = 'Dd', _("D♯")
    Eb = 'Eb', _("E♭")
    E = 'E', _("E")
    F = 'F', _("F")
    Fd = 'Fd', _("F♯")
    Gb = 'Gb', _("G♭")
    G = 'G', _("G")
    Gd = 'Gd', _("G♯")
    Ab = 'Ab', _("A♭")
    A = 'A', _("A")
    Ad = 'Ad', _("A♯")
    Bb = 'Bb', _("B♭")
    B = 'B', _("B")


class Stand(models.Model):
    """
    A set of instruments.

    For example, the trumpet, flugelhorn and cornet instruments could be grouped
    under a « Trumpets » stand.
    """

    name = models.CharField(_("name"), max_length=50)

    class Meta:
        verbose_name = _("stand")
        verbose_name_plural = _("stands")

    def __str__(self):
        return self.name


class Instrument(models.Model):
    """
    An instrument played by the band. It is defined by a name and an optional
    pitch and belongs to a `Stand`.
    """

    name = models.CharField(_("name"), max_length=50)
    pitch = models.CharField(
        _("pitch"), max_length=2, blank=True, choices=Note.choices
    )
    stand = models.ForeignKey(
        Stand,
        on_delete=models.PROTECT,
        related_name='instruments',
        related_query_name='instrument',
        verbose_name=_("stand"),
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['name', 'pitch', 'stand'], name='unique_instrument'
            )
        ]
        verbose_name = _("instrument")
        verbose_name_plural = _("instruments")

    def __str__(self):
        if not self.pitch:
            return self.name
        return gettext("%(name)s in %(pitch)s") % {
            'name': self.name,
            'pitch': self.get_pitch_display(),
        }

class Plays(models.Model):

    user = models.ManyToManyField(
        User, blank=True , editable=False
    )

    nameInstrument = models.ManyToManyField(
        Instrument , blank=True , editable=False
    )


    
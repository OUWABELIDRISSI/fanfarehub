import factory
from factory.django import DjangoModelFactory


class StandFactory(DjangoModelFactory):
    name = factory.Sequence(lambda x: 'Stand #{}'.format(x))

    class Meta:
        model = 'band.Stand'


class InstrumentFactory(DjangoModelFactory):
    name = factory.Sequence(lambda x: 'Instrument #{}'.format(x))
    stand = factory.SubFactory(StandFactory)

    class Meta:
        model = 'band.Instrument'

from django.core.exceptions import NON_FIELD_ERRORS, ValidationError

import pytest

from fanfarehub.band.models import Instrument, Note

from .factories import InstrumentFactory, StandFactory


@pytest.mark.django_db
class TestInstrument:
    def test_str(self):
        instrument = Instrument(name="Clarinet", stand=StandFactory())
        assert str(instrument) == "Clarinet"

        instrument.pitch = Note.A
        assert str(instrument) == "Clarinet in A"

    def test_unique(self):
        stand = StandFactory()
        InstrumentFactory(name="Clarinet", pitch=Note.A, stand=stand)

        with pytest.raises(ValidationError) as excinfo:
            Instrument(name="Clarinet", pitch=Note.A, stand=stand).full_clean()
        assert (
            excinfo.value.error_dict[NON_FIELD_ERRORS][0].code
            == 'unique_together'
        )

        Instrument(name="Clarinet", stand=StandFactory()).full_clean()

        Instrument(name="Clarinet", stand=stand).full_clean()
        Instrument(name="Clarinet", pitch=Note.Bb, stand=stand).full_clean()

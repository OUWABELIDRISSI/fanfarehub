# Generated by Django 2.2.12 on 2020-06-01 16:19

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Stand',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, verbose_name='stand name')),
            ],
            options={
                'verbose_name': 'stand',
                'verbose_name_plural': 'stands',
            },
        ),
        migrations.CreateModel(
            name='Instrument',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, verbose_name='instrument name')),
                ('pitch', models.CharField(max_length=2, verbose_name='pitch')),
                ('key', models.CharField(max_length=8, verbose_name='key')),
                ('stand', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='band.Stand')),
            ],
            options={
                'verbose_name': 'instrument',
                'verbose_name_plural': 'instruments',
            },
        ),
    ]

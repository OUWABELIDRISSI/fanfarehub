from django.contrib import admin

from .models import Profile, User


class UserProfileInline(admin.StackedInline):
    model = Profile
    max_num = 1
    can_delete = False


class UserAdmin(admin.ModelAdmin):
    inlines = [UserProfileInline]


# register new user admin
admin.site.register(User, UserAdmin)
from django.urls import path

from . import views

from django.views.generic.base import TemplateView
from django.contrib.auth.decorators import permission_required

app_name = 'accounts'

urlpatterns = [
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', views.LogoutView.as_view(), name='logout'),

    path('password/reset/',
         views.PasswordResetView.as_view(),
         name='password_reset'),
    path('password/reset/<uidb64>/<token>/',
         views.PasswordResetConfirmView.as_view(),
         name='password_reset_confirm'),
    path('password/',
         views.PasswordChangeView.as_view(),
         name='password_change'),

    path('profile/', views.ProfileEditView.as_view(), name='profile'),
        
    ###############################################    
    path('register/', views.UserRegistrationView.as_view(), name='register'),
    
    path('register/complete/',
         TemplateView.as_view(
            template_name='registration/registration_complete.html'
         ),
         name='registration_complete'),
    path('register/activate/<str:activation_key>/',
         views.ActivationView.as_view(),
         name='registration_activate'),
    path('register/activatation/complete/',
         TemplateView.as_view(
            template_name='registration/activation_complete_admin_pending.html'
         ),
         name='registration_activation_complete'),
    path('register/activatation/resend/',
         views.ResendActivationView.as_view(),
         name='registration_resend_activation'),
    path('register/approve/<int:profile_id>/',
         permission_required('is_superuser')(views.ApprovalView.as_view()),
         name='registration_admin_approve'),
    path('register/approval/complete/',
         TemplateView.as_view(
            template_name='registration/admin_approve_complete.html'
         ),
         name='registration_approve_complete'),

    #####################################################
] 
 
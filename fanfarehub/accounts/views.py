from django.conf import settings
from django.contrib import messages
from django.contrib.auth import views as auth_views
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.utils.translation import gettext
from django.utils.translation import gettext_lazy as _
from django.views.generic.edit import UpdateView




from registration.backends.admin_approval import views as registration_views
from registration.models import SupervisedRegistrationProfile

from .forms import (
    AuthenticationForm,
    PasswordChangeForm,
    PasswordResetForm,
    ProfileEditForm,
    SetPasswordForm,
    UserRegistrationForm,
)
from .models import User

from .models import Profile, User


class LoginView(auth_views.LoginView):
    """
    Logs a user in or link to registration and password reset.
    """

    form_class = AuthenticationForm
    template_name = 'accounts/login.html'
    extra_context = {'title': _("Sign in")}
    redirect_authenticated_user = True


class LogoutView(auth_views.LogoutView):
    """
    Logs the user out.
    """

    def dispatch(self, request, *args, **kwargs):
        response = super().dispatch(request, *args, **kwargs)
        messages.success(request, gettext("You have been logged out."))
        return response


class PasswordResetView(auth_views.PasswordResetView):
    """
    Allows a user to reset their password by generating and emailing a one-time
    use link.
    """

    form_class = PasswordResetForm
    email_template_name = 'accounts/password/reset_email.txt'
    subject_template_name = 'accounts/password/reset_email_subject.txt'
    success_url = settings.LOGIN_URL
    template_name = 'accounts/password/reset.html'
    title = _("Password reset")

    def get(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            return redirect('accounts:password_change')
        return super().get(*args, **kwargs)

    def form_valid(self, form):
        response = super().form_valid(form)
        messages.info(
            self.request,
            gettext(
                "An email has been sent to the address you entered, if it "
                "is attached to a valid account. It contains instructions "
                "for setting your password."
            ),
        )
        return response


class PasswordResetConfirmView(auth_views.PasswordResetConfirmView):
    """
    Confirms the password reset with a form for entering a new one.
    """

    form_class = SetPasswordForm
    success_url = settings.LOGIN_URL
    template_name = 'accounts/password/reset_confirm.html'
    title = _("Password reset")

    def form_valid(self, form):
        response = super().form_valid(form)
        messages.success(
            self.request, gettext("Your password has been changed.")
        )
        return response

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # force the title on the error page
        context['title'] = self.title
        return context


class PasswordChangeView(auth_views.PasswordChangeView):
    """
    Changes the password of the currently logged in user.
    """

    form_class = PasswordChangeForm
    success_url = reverse_lazy('accounts:profile')
    template_name = 'accounts/password/change.html'
    extra_context = {'title': _("Change my password")}


class ProfileEditView(LoginRequiredMixin, UpdateView):
    """
    Updates the profile of the currently logged in user.
    """

    model = User
    form_class = ProfileEditForm
    template_name = 'accounts/profile_edit.html'
    extra_context = {'title': _("My profile")}

    def get_object(self):
        return self.request.user

    def get_success_url(self):
        return self.request.path


class UserRegistrationView(registration_views.RegistrationView):
    """
    Create User account with profile and Group
    User have to confirm email and admin confirm User in a second time
    """

    form_class = UserRegistrationForm
    registration_profile = SupervisedRegistrationProfile
    template_name = 'registration/registration_form.html'
    title = _("Account creation")
    success_url = reverse_lazy('accounts:registration_complete')

    def register(self, form):
        new_user = super(UserRegistrationView, self).register(form)
        # attach profile
        Profile.objects.create(user=new_user)
        # set phone
        new_user.profile.phone = form.cleaned_data['phone']
        new_user.profile.save()

        return new_user


class ActivationView(registration_views.ActivationView):

    registration_profile = SupervisedRegistrationProfile

    def activate(self, *args, **kwargs):
        user = super().activate(*args, **kwargs)
        return user

    def get_success_url(self, user):
        return ('accounts:registration_activation_complete', (), {})


class ApprovalView(registration_views.ApprovalView):
    def approve(self, *args, **kwargs):
        user = super().approve(*args, **kwargs)
        print(user)
        return user

    def get_success_url(self, user):
        return ('accounts:registration_approve_complete', (), {})


class ResendActivationView(registration_views.ResendActivationView):
    template_name = 'registration/resend_activation_form'

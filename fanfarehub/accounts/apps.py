from django.apps import AppConfig


class AccountsConfig(AppConfig):
    name = 'fanfarehub.accounts'
    verbose_name = "Accounts"

# Deployment guide for fanfarehub #

[Fanfarehub](https://framagit.org/labrigadedestubes/fanfarehub) is a web
application to manage a brass band.

## Minimal required environment ##

This deployment guide has been written for Ubuntu 20.10. The following additional
softwares are required:

- Python3, the simple python version management,
  [virtualenv](https://docs.python.org/fr/3/library/venv.html) is highly recommended
- [MySQL](https://www.mysql.com/fr) 8.x
- a code editor, for example [VSCode](https://code.visualstudio.com/)

## Procedure ##

In the rest of this document, we explain the different steps to run and test the
web application. Firstly, a dedicated user and database must be created within the
database management system MySQL. Secondly, the web application must be setup.
Thirdly, the web administrator must be created. 

Please run the next command lines and SQL requests in a terminal 
at the root of the projet.

### <a name="step1">Step 1: MySQL setup</a> ###

Before setting up the web application, you need to create a dedicated MySQL database
in order to store the web application data. 

1. Start the mysql client and connect to a default user:

```
    $sudo mysql --defaults-file=/etc/mysql/debian.cnf

```

2. Create a default MySQL user for the web application

```
    mysql> CREATE USER 'fanfarehub' IDENTIFIED BY 'fanfarehub';

```

3. Create the MySQL database to store the web application data

```

    mysql> CREATE DATABASE fanfarehub_db;

```

4. Grant the user for accessing and modifying the data

```
    mysql> GRANT ALL PRIVILEGES ON fanfarehub_db.* TO 'fanfarehub';

```

5. In order to be able to connect the application to MySQL, you need to install
   `pymysql`. 

```
    $pip install wheel
    $pip install pymysql 

```

6. In the file `venv/lib/python3.X/site-packages/django/db/backends/mysql/base.py`, 
replace the `import MySQLdb as Database`  by the next two lines

```
     import pymysql as Database
     Database.install_as_MySQLdb()

```

### <a name="step2">Step 2: Web application setup</a> ###

The configuration file for the web application deployment which is available at
the root of the project is called `config.env`. The following global variables
must necessarily be setup:

- `DJANGO_SECRET_KEY` is the secret key used to provide cryptographic signing
- `DJANGO_ALLOWED_HOSTS` is the host/domain names for accessing the
  application, for instance `DJANGO_ALLOWED_HOSTS=['*']`
- `DEFAULT_FROM_EMAIL` is the administrator email address, for instance
  `DEFAULT_FROM_EMAIL=name@organisation.org` 
- `DJANGO_DATABASE_URL` is the database backend
- `DJANGO_EMAIL_URL` the URL of the SMTP server 

Firstly, you must generate a cryptographic key with the following command line:

```

    $head -c50 /dev/urandom | base64
    Rs+O8zIn48Xn76pdSfS2DIHRW4rGJtDGpLJ0RqMrni9BPQKwNFN54UUfpf/aAiDmD60= 

```

This `DJANGO_SECRET_KEY` variable must be instantiated with this key in the configuration file.

```

    DJANGO_SECRET_KEY=Rs+O8zIn48Xn76pdSfS2DIHRW4rGJtDGpLJ0RqMrni9BPQKwNFN54UUfpf/aAiDmD60= 

```

Secondly, The `DJANGO_DATABASE_URL` variable must be instantiated with the URI in conformance with
the database configuration of [step 1](#step1).

```

    DJANGO_DATABASE_URL=mysql://fanfarehub:fanfarehub@127.0.0.1:3306/fanfarehub_db

```

Thirdly , to be able to send emails from the application , you need to set up a SMTP server . Visit the following link [Configure your SMTP server](https://infotuto.univ-lille.fr/fiche/zimbra-parametres) to know about how to configure
your SMTP server. After configuring the SMTP server ,in the config file , change the `DJANGO_EMAIL_URL` variable to :
`smtps://emailaddress:password@nameofsmtpserver:port` 

After setting up the global variables , go to a terminal and run the following command to give the user the permission to read the configuration 
file and write into it : 
```
    $ chmod 600 config.env

```

### <a name="step3">Step 3: Run the Web Application</a> ###


1. Create a subdirectory to store the static resources of the application:

```
    $ mkdir fanfarehub/static

```

2. Activate the virtual environment 

```
    $ source venv/bin/activate
```

3. Initialize the web application

```
    $ make init

```

The Django objects are serialized in the database.

4. Run the web server

```
    $make serve 

```

The application is available at the URI [http://127.0.0.1:8000](http://127.0.0.1:8000).
Your browser will follow the redirection toward http://127.0.0.1:8000/accounts/login/

### <a name="step4">Step 3: Use the Web Application</a> ###

1. The web administrator must be created by specifying the credentials (name,
mail, password) and restart the web server

```
    $python manage.py createsuperuser
    $make serve 

```

At this point you can only use the application to update you profile.

2. If you are disconnected, you can access to the registration form
   [http://127.0.0.1:8000/accounts/register](http://127.0.0.1:8000)


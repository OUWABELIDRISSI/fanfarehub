# -*- mode: makefile-gmake -*-
## Variables
# Name of the Python executable
PYTHON_EXE := python3
# Whether virtual environment must be used (y or n)
USE_VENV := y
# Configuration of the virtual environment
VENV_DIR := venv
VENV_OPT := --system-site-packages

# Set the full path to executables
PYTHON_EXE_BASENAME := $(shell basename $(PYTHON_EXE))
VENV_PYTHON := --python=$(PYTHON_EXE_BASENAME)
ifeq ($(USE_VENV), y)
  PYTHON := $(VENV_DIR)/bin/$(PYTHON_EXE_BASENAME)
  PIP := $(VENV_DIR)/bin/pip
else
  PYTHON := $(shell which $(PYTHON_EXE))
  PIP := $(shell which pip)
endif

# Check whether the configuration file must be loaded
ifneq ($(READ_CONFIG_FILE), 0)
  READ_CONFIG_FILE := 1
else
  READ_CONFIG_FILE := 0
endif

# Define the environment to use
ifndef ENV
  ifdef DJANGO_SETTINGS_MODULE
    ENV = $(shell echo $(DJANGO_SETTINGS_MODULE) | cut -d. -f3)
  else
    DEFAULT_ENV := production
    ENV = $(shell \
      sed -n '/^ENV/s/[^=]*=\(.*\)/\1/p' config.env 2> /dev/null \
        | tail -n 1 | grep -Ee '^..*' || echo "$(DEFAULT_ENV)")
  endif
endif

# Define the text editor to use
ifndef EDITOR
  ifdef VISUAL
    EDITOR := $(VISUAL)
  else
    EDITOR := vi
  endif
endif

# RULES -----------------------------------------------------------------------

.PHONY: clean-pyc clean-build clean-static clear-venv help check check-config
.DEFAULT_GOAL := help

# Help is automatically generated with the comment of the rules:
# - # is skipped
# - ## is for production and development
# - ### is for development only
help: ## print this help
ifeq ($(ENV), production)
	@perl -nle'print $& if m{^[a-zA-Z_-]+:[^#]*?## .*$$}' $(MAKEFILE_LIST) \
	  | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-15s\033[0m %s\n", $$1, $$2}'
else
	@perl -nle'print $& if m{^[a-zA-Z_-]+:[^#]*?###? .*$$}' $(MAKEFILE_LIST) \
	  | sort | awk 'BEGIN {FS = ":.*?###? "}; {printf "\033[36m%-15s\033[0m %s\n", $$1, $$2}'
endif

clean: clean-build clean-pyc clean-static ## clean up all temporary files

clean-build:
	rm -rf build/
	rm -rf dist/
	rm -rf *.egg-info

clean-pyc:
	find fanfarehub/ \
	  \( -name '*.pyc' -o -name '*.pyo' -o -name '*~' \) -exec rm -f {} +

clean-static:
	rm -rf var/static

init: create-venv config.env ## initialize the environment and the application
	@$(MAKE) --no-print-directory update

config.env:
	cp config.env.example config.env
	chmod 600 config.env
	$(EDITOR) config.env

update: check-config install-deps migrate static ## update the application and its dependencies
	touch fanfarehub/wsgi.py

check: check-config ## check the configuration and the application
	$(PYTHON) manage.py check

check-config:
	@find . -maxdepth 1 -name 'config\.env' -not -perm 600 -exec false '{}' +  \
	  || { echo "\033[31mError!\033[0m Permissions of 'config.env' look insecure, it should be 600."; \
		exit 1; }

install-deps:
	$(PIP) install --upgrade --requirement requirements/$(ENV).txt

migrate:
	$(PYTHON) manage.py migrate

static:
ifeq ($(ENV), production)
	@echo "Collecting static files…"
	$(PYTHON) manage.py collectstatic --no-input --verbosity 0
endif

## Virtual environment

create-venv: $(PYTHON)

$(PYTHON):
ifeq ($(USE_VENV), y)
	virtualenv $(VENV_OPT) $(VENV_PYTHON) $(VENV_DIR)
else
	@echo "\033[31mErreur!\033[0m Unable to find Python executable '$(PYTHON)'"
	@exit 1
endif

clear-venv:
	-rm -rf $(VENV_DIR)

## Development

serve: ### start a local Web server
	$(PYTHON) manage.py runserver

test: ### run tests of the application
	$(PYTHON) -m pytest --cov --cov-report=term:skip-covered

coverage: test ### check the code coverage
	$(PYTHON) -m coverage html
	@echo open htmlcov/index.html

lint: ### check the code syntax
	@$(PYTHON) -m flake8 fanfarehub || \
	  { echo "\033[31mError!\033[0m Please format the code with: make format"; false; }

format: ### format the code syntax
	$(PYTHON) -m isort fanfarehub
	$(PYTHON) -m black fanfarehub
